﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Resources;
using API.CLASSES;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductosController : ControllerBase
    {
        private readonly ISanJoseService _service;

        public ProductosController(ISanJoseService service)
        {
            _service = service;
        }

        // GET: api/Productos
        [HttpGet]
        public List<Producto> ObtenerProductos()
        {
            return _service.ObtenerProductos();
        }

        // GET: api/Productos/303810
        [HttpGet("{codigo}")]
        public Producto ObtenerProductoPorCodigo(string codigo)
        {
            return _service.ObtenerProductoPorCodigo(codigo);
        }

        // GET: api/Productos/criticos
        [HttpGet("criticos")]
        public List<Producto> ObtenerProductosConStockCritico()
        {
            return _service.ObtenerProductosConStockCritico();
        }

        // POST: api/Productos
        [HttpPost]
        public Producto GenerarProducto([FromBody] Producto producto)
        {
            return _service.GenerarProducto(producto);
        }

        // PUT: api/Productos
        [HttpPut]
        public Producto ModificarProducto([FromBody] Producto producto)
        {
            return _service.ModificarProducto(producto);
        }

    }
}
