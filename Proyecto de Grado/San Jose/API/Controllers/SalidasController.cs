﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Resources;
using API.CLASSES;
using API.DTOs;

namespace API.Controllers

{
    [ApiController]
    [Route("api/[controller]")]
    public class SalidasController : ControllerBase
    {
        private readonly ISanJoseService _service;

        public SalidasController(ISanJoseService service)
        {
            _service = service;
        }

        // GET: api/Salidas
        [HttpGet]
        public List<RapidMinerExportDTO> ObtenerSalidas()
        {
            return _service.ObtenerSalidas();
        }

        // GET: api/Salidas/303810
        [HttpGet("{codigo}")]
        public List<Salida> ObtenerSalidasPorCodigo(string codigo)
        {
            return _service.ObtenerSalidasPorCodigo(codigo);
        }

        //GET: api/Salidas/fecha
        [HttpGet("fecha={fecha}")]
        public List<Salida> ObtenerSalidasPorFecha(string fecha)
        {
            return _service.ObtenerSalidasPorFecha(fecha);
        }

        // GET: api/Salidas/303810/fecha
        [HttpGet("{codigo}/fecha={fecha}")]
        public List<Salida> ObtenerSalidasPorFechaCodigo(string fecha, string codigo)
        {
            return _service.ObtenerSalidasPorFechaCodigo(fecha, codigo);
        }

        // POST: api/Entradas
        [HttpPost]
        public Salida GenerarSalida([FromBody] Salida salida)
        {
            return _service.GenerarSalida(salida);
        }

    }
}
