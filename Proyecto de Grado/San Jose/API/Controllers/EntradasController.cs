﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Resources;
using API.CLASSES;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EntradasController : ControllerBase
    {
        private readonly ISanJoseService _service;

        public EntradasController(ISanJoseService service)
        {
            _service = service;
        }

        // GET: api/Entradas
        [HttpGet]
        public List<Entrada> ObtenerEntradas()
        {
            return _service.ObtenerEntradas();
        }

        // GET: api/Entradas/303810
        [HttpGet("{codigo}")]
        public List<Entrada> ObtenerEntradasPorCodigo(string codigo)
        {
            return _service.ObtenerEntradasPorCodigo(codigo);
        }

        //GET: api/Entradas/fecha
        [HttpGet("fecha={fecha}")]
        public List<Entrada> ObtenerEntradasPorFecha(string fecha)
        {
            return _service.ObtenerEntradasPorFecha(fecha);
        }

        // GET: api/Entradas/303810/fecha
        [HttpGet("{codigo}/fecha={fecha}")]
        public List<Entrada> ObtenerEntradasPorFechaCodigo(string fecha, string codigo)
        {
            return _service.ObtenerEntradasPorFechaCodigo(fecha, codigo);
        }

        // POST: api/Entradas
        [HttpPost]
        public Entrada GenerarEntrada([FromBody] Entrada entrada)
        {
            return _service.GenerarEntrada(entrada);
        }
    }
}
