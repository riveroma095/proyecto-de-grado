﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Resources;
using API.CLASSES;
using API.DTOs;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PrediccionesController : ControllerBase
    {
        private readonly ISanJoseService _service;

        public PrediccionesController(ISanJoseService service)
        {
            _service = service;
        }

        // GET: api/Predicciones
        [HttpGet]
        public List<Producto> ObtenerProductosConPrediccion()
        {
            return _service.ObtenerProductosConPrediccion();
        }

        // GET: api/Predicciones/1
        [HttpGet("{productoId}")]
        public List<PrediccionesTablaDTO> ObtenerPredicciones(int productoId)
        {
            return _service.ObtenerPredicciones(productoId);
        }
    }
}
