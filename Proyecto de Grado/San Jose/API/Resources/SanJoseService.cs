﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.CLASSES;
using API.DTOs;
using Microsoft.EntityFrameworkCore;

namespace API.Resources
{
    public class SanJoseService : ISanJoseService
    {
        private readonly SanJoseContext _context;
        public SanJoseService (SanJoseContext context)
        {
            this._context = context;
        }

        #region Productos
        public List<Producto> ObtenerProductos() 
        {
            return _context.Productos.OrderBy(p => p.nombre).ToList();
        }

        public Producto ObtenerProductoPorCodigo(string codigo)
        {
            var producto = _context.Productos.Where(p => p.codigo.Equals(codigo)).FirstOrDefault();
            producto = GenerarPrediccionesProduto(producto);
            return producto;
        }

        public Producto ObtenerProductoPorId(int productoId, bool necesitoPredicciones = true)
        {
            var producto = _context.Productos.Where(p => p.productoId.Equals(productoId)).FirstOrDefault();
            if (necesitoPredicciones)
            {
                producto = GenerarPrediccionesProduto(producto);
            }    
            return producto;
        }

        public List<Producto> ObtenerProductosConPrediccion()
        {
            List<Producto> productos =_context.Productos.ToList();
            productos.ForEach(p => GenerarPrediccionesProduto(p));
            List<Producto> productoPrediccion = productos.Where(p => p.predicciones.Any()).OrderBy(p => p.nombre).ToList();
            return productoPrediccion;
        }

        public List<Producto> ObtenerProductosConStockCritico()
        {
            List<Producto> productos = _context.Productos.ToList();
            productos.ForEach(p => GenerarPrediccionesProduto(p));
            List<Producto> productoPrediccion = productos.Where(p => p.predicciones.Any()).OrderBy(p => p.nombre).ToList();
            List<Producto> productoCritico = new List<Producto>();
            foreach (var prod in productoPrediccion)
            {
                int cantidad = prod.predicciones.GetRange(0,3).Sum(p => p.cantidad);
                if(cantidad >= prod.stock)
                {
                    productoCritico.Add(prod);
                }
            }
            return productoCritico.ToList();
        }

        public Producto GenerarProducto(Producto prod)
        {
            var producto = new Producto(prod);
            _context.Productos.Add(producto);
            _context.SaveChanges();
            return producto;
        }

        public Producto ModificarProducto(Producto producto)
        {
            if (_context.Productos.Contains(producto))
            {
                _context.Entry(producto).State = EntityState.Modified;
                _context.SaveChanges();
                return producto;
            }
            else
            {
                return GenerarProducto(producto);
            }
            
        }

        #endregion

        #region Entradas
        public List<Entrada> ObtenerEntradas()
        {
            var entradas = GenerarProductosEntradas(_context.Entradas.OrderByDescending(e => e.fecha).ToList());
            return entradas;
        }

        public List<Entrada> ObtenerEntradasPorCodigo(string codigo)
        {
            var entradas = _context.Entradas.Where(e => e.producto.codigo.Equals(codigo)).OrderByDescending(e => e.fecha).ToList();
            entradas = GenerarProductosEntradas(entradas);
            return entradas;
        }

        public List<Entrada> ObtenerEntradasPorFecha(string fecha)
        {
            var dif = fecha.LastIndexOf('-') - fecha.IndexOf('-') - 1;
            var year = fecha.Substring(0, fecha.IndexOf('-'));
            var month = fecha.Substring(fecha.IndexOf('-') + 1, dif);
            var day = fecha.Substring(fecha.LastIndexOf('-') + 1);
            var entradas = _context.Entradas.Where(e => e.fecha.Day.ToString() == day && 
                                                   e.fecha.Month.ToString() == month && 
                                                   e.fecha.Year.ToString() == year).OrderByDescending(e => e.fecha);
            var entradasOrdenadas = GenerarProductosEntradas(entradas.ToList());
            return entradasOrdenadas;
        }

        public List<Entrada> ObtenerEntradasPorFechaCodigo(string fecha, string codigo)
        {
            var dif = fecha.LastIndexOf('-') - fecha.IndexOf('-') - 1;
            var year = fecha.Substring(0, fecha.IndexOf('-'));
            var month = fecha.Substring(fecha.IndexOf('-') + 1, dif);
            var day = fecha.Substring(fecha.LastIndexOf('-') + 1);
            var entradas = _context.Entradas.Where(e => e.fecha.Day.ToString() == day && 
                                                        e.fecha.Month.ToString() == month && 
                                                        e.fecha.Year.ToString() == year &&
                                                        e.producto.codigo.Equals(codigo)).OrderByDescending(e => e.fecha);
            var entradasOrdenadas = GenerarProductosEntradas(entradas.ToList());
            return entradasOrdenadas;
        }

        public Entrada GenerarEntrada(Entrada ent)
        {
            var entrada = new Entrada(ent);
            _context.Entradas.Add(entrada);
            Producto producto = ent.producto;
            producto.stock += ent.cantidad;
            _context.Productos.Update(producto);
            _context.SaveChanges();
            return entrada;
        }
        #endregion

        #region Salidas

        public List<Salida> ObtenerSalidasPorCodigo(string codigo)
        {
            var salidas = _context.Salidas.Where(s => s.producto.codigo.Equals(codigo)).OrderByDescending(s => s.fecha).ToList();
            salidas = GenerarProductosSalidas(salidas);
            return salidas;
        }

        public List<Salida> ObtenerSalidasPorFecha(string fecha)
        {
            var dif = fecha.LastIndexOf('-') - fecha.IndexOf('-') - 1;
            var year = fecha.Substring(0, fecha.IndexOf('-'));
            var month = fecha.Substring(fecha.IndexOf('-') + 1, dif);
            var day = fecha.Substring(fecha.LastIndexOf('-') + 1);
            var salidas = _context.Salidas.Where(s => s.fecha.Day.ToString() == day && 
                                                 s.fecha.Month.ToString() == month && 
                                                 s.fecha.Year.ToString() == year).OrderByDescending(s => s.fecha);
            var salidasOrdenadas = GenerarProductosSalidas(salidas.ToList());
            return salidasOrdenadas;
        }

        public List<Salida> ObtenerSalidasPorFechaCodigo(string fecha, string codigo)
        {
            var dif = fecha.LastIndexOf('-') - fecha.IndexOf('-') - 1;
            var year = fecha.Substring(0, fecha.IndexOf('-'));
            var month = fecha.Substring(fecha.IndexOf('-') + 1, dif);
            var day = fecha.Substring(fecha.LastIndexOf('-') + 1);
            var salidas = _context.Salidas.Where(s => s.fecha.Day.ToString() == day &&
                                                 s.fecha.Month.ToString() == month &&
                                                 s.fecha.Year.ToString() == year &&
                                                 s.producto.codigo.Equals(codigo)).OrderByDescending(s => s.fecha);
            var salidasOrdenadas = GenerarProductosSalidas(salidas.ToList());
            return salidasOrdenadas;
        }

        public Salida GenerarSalida(Salida sal)
        {
            var salida = new Salida(sal);
            _context.Salidas.Add(salida);
            Producto producto = sal.producto;
            producto.stock -= sal.cantidad;
            _context.Productos.Update(producto);
            _context.SaveChanges();
            return salida;
        }
        #endregion

        #region Predicciones
        public List<PrediccionesTablaDTO> ObtenerPredicciones(int productoId)
        {
            List<Prediccion> predicciones = _context.Predicciones.Where(pr => pr.productoId.Equals(productoId)).OrderBy(p => p.fecha).ToList();
            List<PrediccionesTablaDTO> prediccionesTablaDTOs = new List<PrediccionesTablaDTO>();
            for(var i=0; i<predicciones.Count(); i+=3)
            {
                PrediccionesTablaDTO dto = new PrediccionesTablaDTO();
                dto.fecha = predicciones[i].fecha;
                dto.semana1 = predicciones[i].cantidad;
                dto.semana2 = predicciones[i+1].cantidad;
                dto.semana3 = predicciones[i+2].cantidad;
                prediccionesTablaDTOs.Add(dto);
            }
            return prediccionesTablaDTOs.OrderByDescending(p => p.fecha).ToList();
        }

        public Prediccion GenerarPrediccion(Prediccion pred)
        {
            var prediccion = new Prediccion(pred);
            _context.Predicciones.Add(prediccion);
            _context.SaveChanges();
            return prediccion;
        }

        public List<Prediccion> ModificarPrediccion(List<Prediccion> predicciones)
        {
            bool nuevasPreds = false;
            List<Prediccion> preds = new List<Prediccion>();
            foreach (var item in predicciones)
            {
                if (_context.Predicciones.Contains(item))
                {
                    _context.Entry(item).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    nuevasPreds = true;
                    preds.Add(GenerarPrediccion(item));
                }
            }
            if (nuevasPreds) 
            {
                return preds;
            }
            else
            {
                return predicciones;
            }
        }

        #endregion

        #region RapidMiner
        public List<RapidMinerExportDTO> ObtenerSalidas()
        {
            IEnumerable<Salida> salidas = _context.Salidas.OrderByDescending(s => s.fecha);
            IEnumerable<Producto> productos = this.ObtenerProductos();
            IEnumerable<Semana> semanas = _context.Semanas.OrderBy(se => se.numeroSemana).ToList();
            List<RapidMinerExportDTO> result = new List<RapidMinerExportDTO>();
            foreach(var salida in salidas)
            {
                RapidMinerExportDTO dto = new RapidMinerExportDTO();
                dto.codigo = productos.Where(p => p.productoId.Equals(salida.productoId)).Select(p => p.codigo).FirstOrDefault();
                dto.nombre = productos.Where(p => p.productoId.Equals(salida.productoId)).Select(p => p.nombre).FirstOrDefault();
                dto.cantidad = salida.cantidad;
                dto.semana = semanas.Where(se => salida.fecha <= se.fechaFin && salida.fecha >= se.fechaInicio).Select(se => se.numeroSemana).FirstOrDefault();
                result.Add(dto);
            }
            return result;
        }
        #endregion

        #region Metodos privados 
        private List<Entrada> GenerarProductosEntradas(List<Entrada> entradas)
        {
            foreach (var item in entradas)
            {
                item.producto = ObtenerProductoPorId(item.productoId, false);
            }
            return entradas;
        }

        private List<Salida> GenerarProductosSalidas(List<Salida> salidas)
        {
            foreach (var item in salidas)
            {
                item.producto = ObtenerProductoPorId(item.productoId, false);
            }
            return salidas;
        }

        private List<Prediccion> ObtenerPrediccion(int productoId)
        {
            return _context.Predicciones.Where(pr => pr.productoId.Equals(productoId)).OrderByDescending(pr => pr.fecha).ToList();
        }

        private Producto GenerarPrediccionesProduto(Producto producto)
        {
            if (producto != null) 
            {
                var predicciones = ObtenerPrediccion(producto.productoId);
                producto.predicciones = predicciones;
                return producto;
            }
            else
            {
                return producto;
            }
        }

        #endregion

    }
}
