﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using API.CLASSES;

namespace API.Resources
{
    public class SanJoseContext : DbContext
    {
        public SanJoseContext(DbContextOptions<SanJoseContext>options):base(options)
        { 
        }

        public DbSet<Producto> Productos { get; set; }
        public DbSet<Entrada> Entradas { get; set; }
        public DbSet<Salida> Salidas { get; set; }
        public DbSet<Prediccion> Predicciones { get; set; }
        public DbSet<Semana> Semanas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Producto>().HasKey(p => p.productoId);
            modelBuilder.Entity<Entrada>().HasKey(e => e.entradaId);
            modelBuilder.Entity<Salida>().HasKey(s => s.salidaId);
            modelBuilder.Entity<Prediccion>().HasKey(pr => pr.prediccionId);
            modelBuilder.Entity<Semana>().HasKey(se => se.numeroSemana);

            modelBuilder.Entity<Producto>(ep =>
            {
                ep.Property(p => p.productoId).UseIdentityColumn();
                ep.Property(p => p.codigo).HasColumnType("varchar(20)");
                ep.Property(p => p.nombre).HasColumnType("varchar(50)");
                ep.Property(p => p.presentacion).HasColumnType("varchar(50)");
                ep.Ignore(p => p.predicciones);
            });

            modelBuilder.Entity<Entrada>(ee =>
            {
                ee.Property(e => e.entradaId).UseIdentityColumn();
                ee.Property(e => e.productoId).HasColumnType("int");
                ee.Property(e => e.fecha).HasColumnType("date");
                ee.Property(e => e.cantidad).HasColumnType("int");
                ee.Property(e => e.remito).HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Salida>(es =>
            {
                es.Property(s => s.salidaId).UseIdentityColumn();
                es.Property(s => s.productoId).HasColumnType("int");
                es.Property(s => s.fecha).HasColumnType("date");
                es.Property(s => s.cantidad).HasColumnType("int");
            });

            modelBuilder.Entity<Prediccion>(epr =>
            {
                epr.Property(p => p.prediccionId).UseIdentityColumn();
                epr.Property(p => p.productoId).HasColumnType("int");
                epr.Property(p => p.semana).HasColumnType("int");
                epr.Property(p => p.cantidad).HasColumnType("int");
                epr.Property(p => p.fecha).HasColumnType("date");
            });

            modelBuilder.Entity<Semana>(ese =>
            {
                ese.Property(se => se.numeroSemana).HasColumnType("int");
                ese.Property(se => se.fechaInicio).HasColumnType("date");
                ese.Property(se => se.fechaFin).HasColumnType("date");
            });
        }
    }
}
