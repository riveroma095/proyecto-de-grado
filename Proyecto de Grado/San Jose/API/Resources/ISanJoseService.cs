﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.CLASSES;
using API.DTOs;

namespace API.Resources
{
    public interface ISanJoseService
    {
        #region Productos
        List<Producto> ObtenerProductos();

        Producto ObtenerProductoPorCodigo(string codigo);

        Producto ObtenerProductoPorId(int productoId, bool necesitoPredicciones = true);

        List<Producto> ObtenerProductosConPrediccion();

        List<Producto> ObtenerProductosConStockCritico();

        Producto GenerarProducto(Producto producto);

        Producto ModificarProducto(Producto producto);
        #endregion

        #region Entradas
        List<Entrada> ObtenerEntradas();

        List<Entrada> ObtenerEntradasPorCodigo(string codigo);

        List<Entrada> ObtenerEntradasPorFecha(string fecha);

        List<Entrada> ObtenerEntradasPorFechaCodigo(string fecha, string codigo);

        Entrada GenerarEntrada(Entrada entrada);
        #endregion

        #region Salidas

        List<Salida> ObtenerSalidasPorCodigo(string codigo);

        List<Salida> ObtenerSalidasPorFecha(string fecha);

        List<Salida> ObtenerSalidasPorFechaCodigo(string fecha, string codigo);

        Salida GenerarSalida(Salida salida);
        #endregion

        #region Predicciones
        List<PrediccionesTablaDTO> ObtenerPredicciones(int productoId);

        Prediccion GenerarPrediccion(Prediccion pred);

        List<Prediccion> ModificarPrediccion(List<Prediccion> predicciones);
        #endregion

        #region RapidMiner
        List<RapidMinerExportDTO> ObtenerSalidas();
        #endregion
    }
}
