﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs
{
    public class RapidMinerExportDTO
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public int cantidad { get; set; }
        public int semana { get; set; }
    }
}
