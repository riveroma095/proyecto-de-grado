﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs
{
    public class PrediccionesTablaDTO
    {
        public DateTime fecha { get; set; }
        public int semana1 { get; set; }
        public int semana2 { get; set; }
        public int semana3 { get; set; }
    }
}
