﻿using System;
using System.Collections.Generic;
using System.Text;
using API.RESOURCES;

namespace API.CLASSES.SALIDAS
{
    class SalidasManager : DataBaseManager
    {
        public SalidasManager() { }

        public WriteQuery InsertarSalida(Salida salida)
        {
            sqlCommand.Parameters.Clear();
            sqlCommand.CommandText = SalidasQuery.InsertarSalida;
            sqlCommand.Parameters.AddWithValue("@ProductoId", salida.producto.productoId);
            sqlCommand.Parameters.AddWithValue("@Cantidad", salida.cantidad);
            sqlCommand.Parameters.AddWithValue("@Fecha", salida.fecha);

            return ExecuteWriteQuery(sqlCommand);
        }

        public ReadQuery ObtenerSalida(int productoId)
        {
            sqlCommand.Parameters.Clear();
            sqlCommand.CommandText = SalidasQuery.ObtenerSalida;
            sqlCommand.Parameters.AddWithValue("@ProductoId", productoId);

            return ExecuteReadQuery(sqlCommand);
        }
    }
}
