﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.CLASSES.SALIDAS
{
    public class SalidasQuery
    {
		public const string InsertarSalida = "INSERT INTO Salidas(ProductoId, Cantidad, Fecha) VALUES(@ProductoId, @Cantidad, @Fecha)";
		public const string ObtenerSalida = "SELECT p.Codigo, p.Nombre, p.Presentacion, s.Cantidad, s.Fecha	FROM salidas s inner join Productos p on s.ProductoId = p.ProductoId where p.ProductoId = @ProductoId";
	}
}