﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.CLASSES
{
    public class Salida
    {
        public int salidaId { get; set; }
        public int productoId { get; set; }
        public Producto producto { get; set; }
        public DateTime fecha { get; set; }
        public int cantidad { get; set; }

        public Salida()
        {
        }

        public Salida(Salida salida)
        {
            this.productoId = salida.productoId;
            this.fecha = salida.fecha.ToLocalTime();
            this.cantidad = salida.cantidad;
        
        }

    }
}
