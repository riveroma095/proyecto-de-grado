﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.CLASSES
{
    public class Prediccion
    {
        public int prediccionId { get; set; }
        public int productoId { get; set; }
        public int semana { get; set; }
        public int cantidad { get; set; }
        public DateTime fecha { get; set; }

        public Prediccion()
        {
        }

        public Prediccion(Prediccion prediccion)
        {
            this.productoId = prediccion.productoId;
            this.semana = prediccion.semana;
            this.cantidad = prediccion.cantidad;
            this.fecha = prediccion.fecha;
        }
    }
}
