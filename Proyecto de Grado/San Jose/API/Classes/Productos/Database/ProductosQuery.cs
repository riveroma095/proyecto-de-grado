﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.CLASSES.PRODUCTOS
{
    public class ProductosQuery
    {
        public const string ObtenerProductoCompleto = "select p.Codigo, p.Nombre, p.Presentacion, s.Cantidad, pre.Semana, pre.Cantidad from Productos p inner join Predicciones pre on p.ProductoId = pre.ProductoId  inner join Stock s on s.ProductoId = p.ProductoId where p.ProductoId = @ProductoId";
        public const string ObtenerProductoSimplificado = "select p.Codigo, p.Nombre, p.Presentacion, s.Cantidad from Productos p inner join Stock s on s.ProductoId = p.ProductoId where p.ProductoId = 1";
        public const string MostrarProductoCompletoSimplificado = "select count(*) from Productos p inner join Predicciones pre on pre.ProductoId = p.ProductoId  where p.ProductoId = @ProductoId";
        public const string AumentarStock = "UPDATE Stock Set Cantidad = Cantidad + @Cantidad Where ProductoId = @ProductoId";
        public const string ReducirStock = "UPDATE Stock Set Cantidad = Cantidad - @Cantidad Where ProductoId = @ProductoId";
        public const string ObtenerStock = "SELECT s.Cantidad from Productos p inner join Stock s where p.ProductoId = @ProductoId";
        public const string InsertarProducto = "INSERT INTO Productos(Codigo, Nombre, Presentacion) VALUES(@Codigo, @Nombre, @Presentacion)";
        //public const string ModificarProducto = "UPDATE Productos SET Codigo = @Codigo, Nombre = @Nombre, Presentacion = @Presentacion WHERE ProductoId = @ProductoId";
    }
}
