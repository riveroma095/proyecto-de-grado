﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.CLASSES
{
    public class Producto
    {
        public int productoId { get; set; }
        public string nombre{ get; set; }
        public string codigo { get; set; }
        public string presentacion { get; set; }
        public int stock { get; set; }
        public List<Prediccion> predicciones { get; set; }

        public Producto() 
        { 
        }

        public Producto(Producto producto) 
        {
            this.nombre = producto.nombre;
            this.codigo = producto.codigo;
            this.presentacion = producto.presentacion;
            this.stock = producto.stock;
        }
    }
}