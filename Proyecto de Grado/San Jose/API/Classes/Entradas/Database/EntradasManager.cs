﻿using System;
using System.Collections.Generic;
using System.Text;
using API.RESOURCES;

namespace API.CLASSES.ENTRADAS
{
    public class EntradasManager : DataBaseManager
    {
        public EntradasManager() { }

        public WriteQuery InsertarEntrada(Entrada entrada)
        {
            sqlCommand.Parameters.Clear();
            sqlCommand.CommandText = EntradasQuery.InsertarEntrada;
            sqlCommand.Parameters.AddWithValue("@ProductoId", entrada.producto.productoId);
            sqlCommand.Parameters.AddWithValue("@Cantidad", entrada.cantidad);
            sqlCommand.Parameters.AddWithValue("@Fecha", entrada.fecha);

            return ExecuteWriteQuery(sqlCommand);
        }

        public ReadQuery ObtenerEntrada(int productoId)
        {
            sqlCommand.Parameters.Clear();
            sqlCommand.CommandText = EntradasQuery.InsertarEntrada;
            sqlCommand.Parameters.AddWithValue("@ProductoId", productoId);

            return ExecuteReadQuery(sqlCommand);
        }
    }
}
