﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.CLASSES.ENTRADAS
{
    public class EntradasQuery
    {
		public const string InsertarEntrada = "INSERT INTO Entradas(ProductoId, Cantidad, Fecha) VALUES(@ProductoId, @Cantidad, @Fecha)";
		public const string ObtenerEntrada = "SELECT p.Codigo, p.Nombre, p.Presentacion, e.Cantidad, e.Fecha FROM entradas e inner join Productos p on e.ProductoId = p.ProductoId where p.ProductoId = @ProductoId";
	}
}