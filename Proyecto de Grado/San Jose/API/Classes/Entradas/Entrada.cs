﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.CLASSES
{
    public class Entrada
    {
        public int entradaId { get; set; }
        public int productoId { get; set; }
        public Producto producto { get; set; }
        public DateTime fecha { get; set; }
        public int cantidad { get; set; }
        public string remito { get; set; }

        public Entrada()
        {
        }

        public Entrada(Entrada entrada)
        {
            this.productoId = entrada.productoId;
            this.fecha = entrada.fecha.ToLocalTime();
            this.cantidad = entrada.cantidad;
            this.remito = entrada.remito;
        }
    }
}