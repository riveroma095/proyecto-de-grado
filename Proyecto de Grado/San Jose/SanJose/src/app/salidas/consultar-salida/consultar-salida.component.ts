import { ViewChild, Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { SanJoseService } from 'src/app/san-jose.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Salida } from 'src/app/models/salida';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-consultar-salida',
  templateUrl: './consultar-salida.component.html',
  styleUrls: ['./consultar-salida.component.scss']
})
export class ConsultarSalidaComponent implements OnInit {

  private date = new FormControl('');
  private productoId = new FormControl('');
  public form: FormGroup;
  tablaActiva: boolean;
  seleccionParametros: boolean;
  productos: Producto[];
  salidas: Salida[];
  selectedProducto: Producto;
  selectedFecha: Date;
  displayedColumns: string[] = ['id', 'nombre', 'fecha', 'cantidad'];
  dataSource:any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.tablaActiva = false;
    this.seleccionParametros = true;
    this.service.getProductos().subscribe(response => {
      this.productos = response;
    });
  }

  getSalidas(){
    if (this.productoId.value && !this.date.value)
    {
      this.service.getSalidasPorCodigo(this.selectedProducto.codigo).subscribe(response => {
        this.salidas = response;
        this.dataSource = new MatTableDataSource<Salida>(this.salidas);
        this.dataSource.paginator = this.paginator;
      });        
      this.tablaActiva = true;
    }

    if (!this.productoId.value && this.date.value )
    {
      this.service.getSalidasPorFecha(this.formatFecha(this.selectedFecha)).subscribe(response => {
        this.salidas = response;
        this.dataSource = new MatTableDataSource<Salida>(this.salidas);
        this.dataSource.paginator = this.paginator;
      })
      this.tablaActiva = true;
    }

    if (this.productoId.value && this.date.value)
    {
      this.service.getSalidasPorFechaCodigo(this.formatFecha(this.selectedFecha), this.selectedProducto.codigo).subscribe(response => {
        this.salidas = response;
        this.dataSource = new MatTableDataSource<Salida>(this.salidas);
        this.dataSource.paginator = this.paginator;
      })
      this.tablaActiva = true;
    }

    if(!this.productoId.value && !this.date.value)
    {
      this.seleccionParametros = false;
    }
  }

  getCodigoSelectedSalida(producto){
    this.selectedProducto = this.productos.find(p => p.nombre == producto);
    this.seleccionParametros = true;
  }

  getSelectedFecha(fecha){
    this.selectedFecha = fecha;
    this.seleccionParametros = true;
  }

  limpiar(){
    this.tablaActiva = false;
    this.seleccionParametros = false;
    this.date.setValue('');
    this.productoId.setValue('');
  }

  private initFormGroup(): void {
    this.form = new FormGroup({
      'date': this.date,
      'productoId': this.productoId,
    });
  }

  private formatFecha(fecha: Date): string {
    let year = fecha.getFullYear().toString();
    let month = fecha.getMonth() + 1;
    let day = fecha.getDate().toString();
    let result = year + '-' + month.toString() + '-'+ day
    return result;
  }
}