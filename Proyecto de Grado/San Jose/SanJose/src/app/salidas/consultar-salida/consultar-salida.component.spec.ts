import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarSalidaComponent } from './consultar-salida.component';

describe('ConsultarSalidaComponent', () => {
  let component: ConsultarSalidaComponent;
  let fixture: ComponentFixture<ConsultarSalidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultarSalidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
