import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { SanJoseService } from 'src/app/san-jose.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Salida } from 'src/app/models/salida';

@Component({
  selector: 'app-registrar-salida',
  templateUrl: './registrar-salida.component.html',
  styleUrls: ['./registrar-salida.component.scss']
})
export class RegistrarSalidaComponent implements OnInit {
  private productoId = new FormControl('');
  private cantidad = new FormControl('');
  public form: FormGroup;
  salidaCargada: boolean = false;
  noStock: boolean = false;
  faltanDatos: boolean = false;
  seleccionProducto: boolean = false;
  seleccionCantidad: boolean = false;
  productos: Producto[];
  selectedProducto: Producto;
  salida: Salida;

  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.service.getProductos().subscribe(response => {
      this.productos = response;
    });
  }

  getCodigoSelectedProducto(producto){
    this.selectedProducto = this.productos.find(p => p.nombre == producto);
    this.seleccionProducto = true;
    this.faltanDatos = false;
    this.noStock = false;
  }

  changeCantidad(){
    this.cantidad.value ? this.seleccionCantidad = true : this.seleccionCantidad = false;
    this.faltanDatos = false;
    this.noStock = false;
  }

  postSalida(){
    if(this.seleccionProducto && this.seleccionCantidad)
      {
        if(this.selectedProducto.stock >= this.cantidad.value)
        {
          let salida: Salida = {
            salidaId: 0,
            producto: this.selectedProducto,
            productoId: this.selectedProducto.productoId,
            cantidad: this.cantidad.value,
            fecha: new Date(),
          }
          this.service.addSalida(salida).subscribe(response => {
            this.salidaCargada = true;
          });
        }
        else
        {
          this.noStock = true;
        }
      }
      else
      {
        this.faltanDatos = true;
      }    
  }


  limpiar(){
    this.seleccionProducto = false;
    this.seleccionCantidad = false;
    this.salidaCargada = false;
    this.faltanDatos = false;
    this.noStock = false;
    this.productoId.setValue('');
    this.cantidad.setValue('');
    this.ngOnInit();
  }

  private initFormGroup(): void {
    this.form = new FormGroup({
      'productoId': this.productoId,
      'cantidad': this.cantidad,
    });
  }

}
