import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete'
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableExporterModule } from 'mat-table-exporter';

import { InicioComponent } from './inicio/inicio.component';
import { RegistrarEntradaComponent } from './entradas/registrar-entrada/registrar-entrada.component';
import { ConsultarEntradaComponent } from './entradas/consultar-entrada/consultar-entrada.component';
import { RegistrarSalidaComponent } from './salidas/registrar-salida/registrar-salida.component';
import { ConsultarSalidaComponent } from './salidas/consultar-salida/consultar-salida.component';
import { RegistrarProductoComponent } from './productos/registrar-producto/registrar-producto.component';
import { ConsultarProductoComponent } from './productos/consultar-producto/consultar-producto.component';
import { ModificarProductoComponent } from './productos/modificar-producto/modificar-producto.component';
import { ConsultarPrediccionComponent } from './productos/consultar-prediccion/consultar-prediccion.component';
import { ConsultarStockCriticoComponent } from './productos/consultar-stock-critico/consultar-stock-critico.component';
import { ActualizarModeloComponent } from './productos/actualizar-modelo/actualizar-modelo.component';

@NgModule({
  declarations: [
    AppComponent,
    SidemenuComponent,
    InicioComponent,
    RegistrarEntradaComponent,
    ConsultarEntradaComponent,
    RegistrarSalidaComponent,
    ConsultarSalidaComponent,
    RegistrarProductoComponent,
    ConsultarProductoComponent,
    ModificarProductoComponent,
    ConsultarPrediccionComponent,
    ConsultarStockCriticoComponent,
    ActualizarModeloComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatCheckboxModule,
    MatButtonModule,
    MatAutocompleteModule,
    HttpClientModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatTableExporterModule
  ],
  providers: [
    MatDatepickerModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
