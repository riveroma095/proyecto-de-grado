import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Salida } from './models/salida';
import { Entrada } from './models/entrada';
import { Prediccion, Prediccion_Tabla } from './models/prediccion';
import { Producto, Producto_Prediccion } from './models/producto';
import { RapidMinerExport } from './models/rapidMinerExport';

@Injectable({
  providedIn: 'root'
})
export class SanJoseService {

  private urlProductos = "https://localhost:5001/api/Productos/";
  private urlEntradas = 'https://localhost:5001/api/Entradas/';
  private urlSalidas = 'https://localhost:5001/api/Salidas/';
  private urlPrediccion = "https://localhost:5001/api/Predicciones/";

  constructor(private httpClient: HttpClient) { }

  //Productos
  getProductos(): Observable<Producto[]>{
    return this.httpClient.get<Producto[]>(this.urlProductos);
  }

  getProductoPorCodigo(codigo: string): Observable<Producto_Prediccion>{
    return this.httpClient.get<Producto_Prediccion>(this.urlProductos + codigo);
  }

  getPrediccionPorCodigo(codigo: string): Observable<Prediccion[]>{
    return this.httpClient.get<Prediccion[]>(this.urlProductos + codigo + '/Prediccion')
  }

  getProductosStockCritico(): Observable<Producto_Prediccion[]>{
    return this.httpClient.get<Producto_Prediccion[]>(this.urlProductos + 'criticos')
  }

  addProducto(producto: Producto):Observable<Producto>{
    return this.httpClient.post<Producto>(this.urlProductos, producto)
  }

  updateProducto(producto: Producto):Observable<Producto>{
    return this.httpClient.put<Producto>(this.urlProductos, producto)
  }

  updatePredicciones(predicciones: Prediccion[]): Observable<Prediccion[]>{
    return this.httpClient.put<Prediccion[]>(this.urlPrediccion, predicciones)
  }

  //Entradas
  getEntradas(): Observable<Entrada[]>{
    return this.httpClient.get<Entrada[]>(this.urlEntradas)
  }

  getEntradasPorCodigo(codigo: string): Observable<Entrada[]>{
    return this.httpClient.get<Entrada[]>(this.urlEntradas + codigo)
  }

  getEntradasPorFecha(fecha: string): Observable<Entrada[]>{
    return this.httpClient.get<Entrada[]>(this.urlEntradas + 'fecha=' + fecha)
  }

  getEntradasPorFechaCodigo(fecha: string, codigo:string): Observable<Entrada[]>{
    return this.httpClient.get<Entrada[]>(this.urlEntradas + codigo +'/fecha=' + fecha)
  }

  addEntrada(entrada: Entrada):Observable<Entrada>{
    return this.httpClient.post<Entrada>(this.urlEntradas, entrada)
  }

  //Salidas
  getSalidas(): Observable<RapidMinerExport[]>{
    return this.httpClient.get<RapidMinerExport[]>(this.urlSalidas)
  }

  getSalidasPorCodigo(codigo: string): Observable<Salida[]>{
    return this.httpClient.get<Salida[]>(this.urlSalidas + codigo)
  }

  getSalidasPorFecha(fecha: string): Observable<Salida[]>{
    return this.httpClient.get<Salida[]>(this.urlSalidas + 'fecha=' + fecha)
  }

  getSalidasPorFechaCodigo(fecha: string, codigo:string): Observable<Salida[]>{
    return this.httpClient.get<Salida[]>(this.urlSalidas + codigo +'/fecha=' + fecha)
  }

  addSalida(salida: Salida): Observable<Salida>{
    return this.httpClient.post<Salida>(this.urlSalidas, salida)
  }

  //Predicciones
  getProductosConPrediccion(): Observable<Producto_Prediccion[]>{
    return this.httpClient.get<Producto_Prediccion[]>(this.urlPrediccion)
  }

  getPrediccionesPorId(productoId: number): Observable<Prediccion_Tabla[]>{
    return this.httpClient.get<Prediccion_Tabla[]>(this.urlPrediccion + productoId)
  }

}
