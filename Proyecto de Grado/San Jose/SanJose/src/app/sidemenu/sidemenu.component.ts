import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {

  ocultarProductos: boolean = true;
  ocultarEntradas: boolean = true;
  ocultarSalidas: boolean = true;

  constructor() { }

  inicio = {name:"Inicio", route: "inicio", icon:"home_outlined"}

  producto = {name: "Productos", route:"productos", icon: "list"}

  entrada = {name:"Entradas", route:"entradas", icon:"login"}

  salida = {name:"Salidas", route:"salidas", icon:"logout"}

  productos = [
    {name:"Consultar Producto", route:"consultar-producto"},
    {name:"Registrar Producto", route:"registrar-producto"},
    {name: "Modificar Producto", route: 'modificar-producto'},
    {name: 'Consultar Stock Crítico', route:'consultar-stock-critico'},
    {name: 'Consultar Predicciones', route:'consultar-prediccion'},
    {name: 'Actualizar Modelo', route:'actualizar-modelo'},
  ]

  entradas = [
    {name:"Consultar", route:"consultar-entrada"},
    {name:"Registrar", route:"registrar-entrada"},
  ]

  salidas = [
    {name:"Consultar", route:"consultar-salida"},
    {name:"Registrar", route:"registrar-salida"},
  ]

  ngOnInit(): void {
  }

  open(param: string){
    if(param == 'producto')
    {
      if(this.ocultarProductos)
      {
        this.ocultarProductos = false;
      }
      else
      {
        this.ocultarProductos = true;
      }
    }

    if(param == 'entrada')
    {
      if(this.ocultarEntradas)
      {
        this.ocultarEntradas = false;
      }
      else
      {
        this.ocultarEntradas = true;
      }
    }

    if(param == 'salida')
    {
      if(this.ocultarSalidas)
      {
        this.ocultarSalidas = false;
      }
      else
      {
        this.ocultarSalidas = true;
      }
    }

  }


}
