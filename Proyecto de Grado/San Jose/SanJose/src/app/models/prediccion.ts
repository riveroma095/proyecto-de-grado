export interface Prediccion{
  prediccionId: number;
  productoId: number;
  semana: number;
  cantidad: number;
  fecha: Date;
}

export interface Prediccion_Tabla{
  fecha: Date;
  semana1: number;
  semana2: number;
  semana3: number
}