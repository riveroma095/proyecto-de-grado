export interface RapidMinerExport{
  codigo: string;
  nombre: string;
  cantidad: number;
  semana: number;
}