import {Prediccion} from './prediccion'

export interface Producto_Prediccion{
  productoId: number;
  nombre: string;
  codigo: string;
  presentacion: string;
  stock: number;
  predicciones: Prediccion[]
}

export interface Producto{
  productoId: number;
  nombre: string;
  codigo: string;
  presentacion: string;
  stock: number;
}