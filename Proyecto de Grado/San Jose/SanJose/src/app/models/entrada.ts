import { Producto } from "./producto";

export interface Entrada{
  entradaId: number;
  productoId: number;
  producto: Producto;
  fecha: Date;
  cantidad: number;
  remito: string;
}