import {Producto} from './producto'
export interface Salida{
    salidaId: number;
    productoId: number;
    producto: Producto; 
    fecha: Date;
    cantidad: number
}