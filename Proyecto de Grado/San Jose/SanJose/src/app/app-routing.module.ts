import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultarEntradaComponent } from './entradas/consultar-entrada/consultar-entrada.component';
import { RegistrarEntradaComponent } from './entradas/registrar-entrada/registrar-entrada.component';
import { InicioComponent } from './inicio/inicio.component';
import { ConsultarPrediccionComponent } from './productos/consultar-prediccion/consultar-prediccion.component';
import { ConsultarProductoComponent } from './productos/consultar-producto/consultar-producto.component';
import { ConsultarStockCriticoComponent } from './productos/consultar-stock-critico/consultar-stock-critico.component';
import { ModificarProductoComponent } from './productos/modificar-producto/modificar-producto.component'
import { RegistrarProductoComponent } from './productos/registrar-producto/registrar-producto.component';
import { ConsultarSalidaComponent } from './salidas/consultar-salida/consultar-salida.component';
import { RegistrarSalidaComponent } from './salidas/registrar-salida/registrar-salida.component';
import { ActualizarModeloComponent } from './productos/actualizar-modelo/actualizar-modelo.component';

const routes: Routes = [
  {path:'', component: InicioComponent},
  {path:'inicio', component: InicioComponent},

  {path: 'consultar-producto', component:ConsultarProductoComponent},
  {path: 'registrar-producto', component:RegistrarProductoComponent},
  {path: 'modificar-producto', component:ModificarProductoComponent},
  {path: 'consultar-stock-critico', component: ConsultarStockCriticoComponent},
  {path: 'consultar-prediccion', component: ConsultarPrediccionComponent},
  {path: 'actualizar-modelo', component: ActualizarModeloComponent},

  {path: 'consultar-entrada', component:ConsultarEntradaComponent},
  {path: 'registrar-entrada', component:RegistrarEntradaComponent},

  {path: 'consultar-salida', component:ConsultarSalidaComponent},
  {path: 'registrar-salida', component:RegistrarSalidaComponent},

  {path:'**', component: InicioComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
