import { Component, OnInit } from '@angular/core';
import { RapidMinerExport } from 'src/app/models/rapidMinerExport'
import { SanJoseService } from 'src/app/san-jose.service';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-actualizar-modelo',
  templateUrl: './actualizar-modelo.component.html',
  styleUrls: ['./actualizar-modelo.component.scss']
})
export class ActualizarModeloComponent implements OnInit {

  displayedColumns: string[] = ['codigo', 'nombre', 'cantidad', 'semana'];
  datos: RapidMinerExport[]
  dataSource:any;
  showLabel: boolean = false;
  buttonAbled: boolean = false;
  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.getSalidas();
  }

  getSalidas(){
      this.service.getSalidas().subscribe(response => {
        this.datos = response;
        this.dataSource = new MatTableDataSource<RapidMinerExport>(this.datos);
        this.buttonAbled = true;
      });
    }

    changeLabel(){
      this.showLabel = true;
    }
  }
