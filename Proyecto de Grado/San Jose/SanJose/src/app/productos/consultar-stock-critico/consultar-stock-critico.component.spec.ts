import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarStockCriticoComponent } from './consultar-stock-critico.component';

describe('ConsultarStockCriticoComponent', () => {
  let component: ConsultarStockCriticoComponent;
  let fixture: ComponentFixture<ConsultarStockCriticoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultarStockCriticoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarStockCriticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
