import { Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Producto_Prediccion } from 'src/app/models/producto';
import { SanJoseService } from 'src/app/san-jose.service';

@Component({
  selector: 'app-consultar-stock-critico',
  templateUrl: './consultar-stock-critico.component.html',
  styleUrls: ['./consultar-stock-critico.component.scss']
})
export class ConsultarStockCriticoComponent implements OnInit {
  totalSemanas: number;
  productos: Producto_Prediccion[];
  displayedColumns: string[] = ['nombre', 'codigo' , 'stock', 'prediccion'];
  dataSource:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.totalSemanas = null;
    this.service.getProductosStockCritico().subscribe(response => {
      this.productos = response;
      this.dataSource = new MatTableDataSource<Producto_Prediccion>(this.productos);
      this.dataSource.paginator = this.paginator;
    });
  }

  
}
