import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarPrediccionComponent } from './consultar-prediccion.component';

describe('ConsultarPrediccionComponent', () => {
  let component: ConsultarPrediccionComponent;
  let fixture: ComponentFixture<ConsultarPrediccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultarPrediccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarPrediccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
