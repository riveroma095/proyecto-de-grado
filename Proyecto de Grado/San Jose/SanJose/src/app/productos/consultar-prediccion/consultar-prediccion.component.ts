import { Component, OnInit, ViewChild } from '@angular/core';
import { Producto_Prediccion } from 'src/app/models/producto';
import { SanJoseService } from 'src/app/san-jose.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Prediccion, Prediccion_Tabla } from 'src/app/models/prediccion';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-consultar-prediccion',
  templateUrl: './consultar-prediccion.component.html',
  styleUrls: ['./consultar-prediccion.component.scss']
})
export class ConsultarPrediccionComponent implements OnInit {
  private productoId = new FormControl('');
  public form: FormGroup;
  productos: Producto_Prediccion[];
  selectedProducto: Producto_Prediccion;
  predicciones: Prediccion_Tabla[];
  tablaActiva: boolean;
  displayedColumns: string[] = ['fecha', 'semana 1' , 'semana 2', 'semana 3'];
  dataSource:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.tablaActiva = false;
    this.service.getProductosConPrediccion().subscribe(response => {
      this.productos = response;
    });
  }

  getPrediccionesProducto(){
    this.selectedProducto = this.productos.find(p => p.nombre == this.productoId.value);
    this.service.getPrediccionesPorId(this.selectedProducto.productoId).subscribe(response => {
      console.log(response);
      this.predicciones = response;
      this.dataSource = new MatTableDataSource<Prediccion_Tabla>(this.predicciones);
      this.dataSource.paginator = this.paginator;
    })
    this.tablaActiva = true;
  }

  private initFormGroup(): void {
    this.form = new FormGroup({
      'productoId': this.productoId,
    });
  }

}
