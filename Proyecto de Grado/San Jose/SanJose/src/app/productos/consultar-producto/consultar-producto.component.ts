import { Component, OnInit } from '@angular/core';
import { Producto, Producto_Prediccion } from 'src/app/models/producto';
import { SanJoseService } from  '../../san-jose.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-consultar-producto',
  templateUrl: './consultar-producto.component.html',
  styleUrls: ['./consultar-producto.component.scss']
})
export class ConsultarProductoComponent implements OnInit {
  productos: Producto[]; 
  selectedProducto: Producto;
  productoPrediccion: Producto_Prediccion;
  fechaPrediccion: Date;
  semana1: number;
  semana2: number;
  semana3: number;
  totalSemanas: number;
  seleccionProducto: boolean = false;
  stockCritico: boolean = false;
  hayPrediccion: boolean;
  private productoId = new FormControl('');
  public form: FormGroup;

  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.service.getProductos().subscribe(response => {
      this.productos = response;
    });
  }

  getCodigoSelectedProducto(producto){
    this.semana1 = null;
    this.semana2 = null;
    this.semana3 = null;
    this.totalSemanas = null;
    this.stockCritico = false;
    this.selectedProducto = this.productos.find(p => p.nombre == producto);
    this.service.getProductoPorCodigo(this.selectedProducto.codigo).subscribe(response => {
      this.productoPrediccion = response;
      if(this.productoPrediccion.predicciones.length != 0)
      {
        this.hayPrediccion = true;
        this.semana1 = this.productoPrediccion.predicciones[0].cantidad;
        this.semana2 = this.productoPrediccion.predicciones[1].cantidad;
        this.semana3 = this.productoPrediccion.predicciones[2].cantidad;
        this.totalSemanas = this.semana1 + this.semana2 + this.semana3;
        this.stockCritico = this.totalSemanas >= this.selectedProducto.stock;
        this.fechaPrediccion = this.productoPrediccion.predicciones[0].fecha;
      }
      else{
        this.hayPrediccion = false;
      }
    })
    this.seleccionProducto = true;
  }

  private initFormGroup(): void {
    this.form = new FormGroup({
      'productoId': this.productoId,
    });
  }

}
