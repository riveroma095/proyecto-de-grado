import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { SanJoseService } from 'src/app/san-jose.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-registrar-producto',
  templateUrl: './registrar-producto.component.html',
  styleUrls: ['./registrar-producto.component.scss']
})
export class RegistrarProductoComponent implements OnInit {
  private codigo = new FormControl('');
  private nombre = new FormControl('');
  private presentacion = new FormControl('');
  private stock = new FormControl('');
  public form: FormGroup;
  productoCargado: boolean = false;
  faltanDatos: boolean = false;
  seleccionCodigo: boolean = false;
  seleccionNombre: boolean = false;
  seleccionPresentacion: boolean = false;
  seleccionStock: boolean = false;
  
  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.initFormGroup();
  }

  postProducto(){
    if(this.seleccionCodigo && this.seleccionNombre && this.seleccionPresentacion && this.seleccionStock)
    {
      let prod: Producto = {
        productoId: 0,
        codigo: this.codigo.value,
        nombre: this.nombre.value,
        presentacion: this.presentacion.value,
        stock: this.stock.value
      }
      this.service.addProducto(prod).subscribe(response => {
        this.productoCargado = true;
      });
    }
    else
    {
      this.faltanDatos = true;
    }
  }
  
  changeCodigo(){
    this.codigo.value ? this.seleccionCodigo = true : this.seleccionCodigo = false;
    this.faltanDatos = false;
  }

  changeNombre(){
    this.nombre.value ? this.seleccionNombre = true : this.seleccionNombre = false;
    this.faltanDatos = false;
  }

  changePresentacion(){
    this.presentacion.value ? this.seleccionPresentacion = true : this.seleccionPresentacion = false;
    this.faltanDatos = false;
  }

  changeStock(){
    this.stock.value ? this.seleccionStock = true : this.seleccionStock = false;
    this.faltanDatos = false;
  }

  limpiar(){
    this.seleccionCodigo = false;
    this.seleccionNombre = false;
    this.seleccionPresentacion = false;
    this.seleccionStock = false;
    this.faltanDatos = false;
    this.productoCargado = false;
    this.codigo.setValue('');
    this.nombre.setValue('');
    this.presentacion.setValue('');
    this.stock.setValue('')
  }

  private initFormGroup(): void {
    this.form = new FormGroup({
      'codigo': this.codigo,
      'nombre': this.nombre,
      'presentacion': this.presentacion,
      'stock': this.stock,
    });
  }
}
