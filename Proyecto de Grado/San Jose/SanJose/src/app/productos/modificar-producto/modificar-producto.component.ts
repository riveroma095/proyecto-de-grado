import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { SanJoseService } from 'src/app/san-jose.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-modificar-producto',
  templateUrl: './modificar-producto.component.html',
  styleUrls: ['./modificar-producto.component.scss']
})
export class ModificarProductoComponent implements OnInit {
  private productoId = new FormControl('');
  private codigo = new FormControl('');
  private nombre = new FormControl('');
  private presentacion = new FormControl('');
  private stock = new FormControl('');
  public form: FormGroup;
  productoCargado: boolean = false;
  faltanDatos: boolean = false;
  seleccionProducto: boolean = false;
  seleccionCodigo: boolean = false;
  seleccionNombre: boolean = false;
  seleccionPresentacion: boolean = false;
  seleccionStock: boolean = false;
  productos: Producto[];
  selectedProducto: Producto;
  
  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.service.getProductos().subscribe(response => {
      this.productos = response;
    });
  }

  getCodigoSelectedProducto(producto){
    this.selectedProducto = this.productos.find(p => p.nombre == producto);
    this.codigo.setValue(this.selectedProducto.codigo);
    this.nombre.setValue(this.selectedProducto.nombre);
    this.presentacion.setValue(this.selectedProducto.presentacion);
    this.stock.setValue(this.selectedProducto.stock);
    this.changeCodigo();
    this.changeNombre();
    this.changePresentacion();
    //this.changeStock();
    this.seleccionProducto = true;
  }

  putProducto(){
    //IF(this.seleccionCodigo && this.seleccionNombre && this.seleccionPresentacion && this.seleccionStock)
    if(this.seleccionCodigo && this.seleccionNombre && this.seleccionPresentacion)
    {
      let prod: Producto = {
        productoId: this.selectedProducto.productoId,
        codigo: this.codigo.value,
        nombre: this.nombre.value,
        presentacion: this.presentacion.value,
        stock: this.stock.value
      }
      this.service.updateProducto(prod).subscribe(response => {
        this.productoCargado = true;
      });
    }
    else
    {
      this.faltanDatos = true;
    }
  }
  
  changeCodigo(){
    this.codigo.value ? this.seleccionCodigo = true : this.seleccionCodigo = false;
    this.faltanDatos = false;
  }

  changeNombre(){
    this.nombre.value ? this.seleccionNombre = true : this.seleccionNombre = false;
    this.faltanDatos = false;
  }

  changePresentacion(){
    this.presentacion.value ? this.seleccionPresentacion = true : this.seleccionPresentacion = false;
    this.faltanDatos = false;
  }

  // changeStock(){
  //   this.stock.value ? this.seleccionStock = true : this.seleccionStock = false;
  //   this.faltanDatos = false;
  // }

  limpiar(){
    this.seleccionCodigo = false;
    this.seleccionNombre = false;
    this.seleccionPresentacion = false;
    //this.seleccionStock = false;
    this.faltanDatos = false;
    this.productoCargado = false;
    this.seleccionProducto = false
    this.productoId.setValue('');
    this.codigo.setValue('');
    this.nombre.setValue('');
    this.presentacion.setValue('');
    this.stock.setValue('');
    this.ngOnInit();
  }

  private initFormGroup(): void {
    this.form = new FormGroup({
      'productoId': this.productoId,
      'codigo': this.codigo,
      'nombre': this.nombre,
      'presentacion': this.presentacion,
      'stock': this.stock,
    });
  }
}
