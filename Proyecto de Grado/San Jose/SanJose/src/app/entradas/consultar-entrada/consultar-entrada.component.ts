import { ViewChild, Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { SanJoseService } from 'src/app/san-jose.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Entrada } from 'src/app/models/entrada';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-consultar-entrada',
  templateUrl: './consultar-entrada.component.html',
  styleUrls: ['./consultar-entrada.component.scss']
})
export class ConsultarEntradaComponent implements OnInit {
  private date = new FormControl('');
  private productoId = new FormControl('');
  public form: FormGroup;
  tablaActiva: boolean;
  seleccionParametros: boolean;
  productos: Producto[];
  entradas: Entrada[];
  selectedProducto: Producto;
  selectedFecha: Date;
  displayedColumns: string[] = ['id', 'nombre', 'fecha', 'cantidad', 'remito'];
  dataSource:any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.tablaActiva = false;
    this.seleccionParametros = true;
    this.service.getProductos().subscribe(response => {
      this.productos = response;
    });
  }

  getEntradas(){
    if (this.productoId.value && !this.date.value)
    {
      this.service.getEntradasPorCodigo(this.selectedProducto.codigo).subscribe(response => {
        this.entradas = response;
        this.dataSource = new MatTableDataSource<Entrada>(this.entradas);
        this.dataSource.paginator = this.paginator;
      })
      this.tablaActiva = true;
    }

    if (!this.productoId.value && this.date.value)
    {
      this.service.getEntradasPorFecha(this.formatFecha(this.selectedFecha)).subscribe(response => {
        this.entradas = response;
        this.dataSource = new MatTableDataSource<Entrada>(this.entradas);
        this.dataSource.paginator = this.paginator;
      })
      this.tablaActiva = true;
    }

    if (this.productoId.value && this.date.value)
    {
      this.service.getEntradasPorFechaCodigo(this.formatFecha(this.selectedFecha), this.selectedProducto.codigo).subscribe(response => {
        this.entradas = response;
        this.dataSource = new MatTableDataSource<Entrada>(this.entradas);
        this.dataSource.paginator = this.paginator;
      })
      this.tablaActiva = true;
    }

    if(!this.productoId.value && !this.date.value)
    {
      this.seleccionParametros = false;
    }
  }

  getCodigoSelectedProducto(){
    this.selectedProducto = this.productos.find(p => p.nombre == this.productoId.value);
    this.seleccionParametros = true;
  }

  getSelectedFecha(){
    this.selectedFecha = this.date.value;
    this.seleccionParametros = true;
  }

  limpiar(){
    this.tablaActiva = false;
    this.seleccionParametros = true;
    this.date.setValue('');
    this.productoId.setValue('');
  }

  private initFormGroup(): void {
    this.form = new FormGroup({
      'date': this.date,
      'productoId': this.productoId,
    });
  }

  private formatFecha(fecha: Date): string {
    let year = fecha.getFullYear().toString();
    let month = fecha.getMonth() + 1;
    let day = fecha.getDate().toString();
    let result = year + '-' + month.toString() + '-'+ day
    return result;
  }
}
