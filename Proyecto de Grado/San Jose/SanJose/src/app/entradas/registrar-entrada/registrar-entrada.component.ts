import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { SanJoseService } from 'src/app/san-jose.service';
import { Entrada } from 'src/app/models/entrada';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-registrar-entrada',
  templateUrl: './registrar-entrada.component.html',
  styleUrls: ['./registrar-entrada.component.scss']
})
export class RegistrarEntradaComponent implements OnInit {
  private productoId = new FormControl('');
  private cantidad = new FormControl('');
  private remito = new FormControl('');
  public form: FormGroup;
  entradaCargada: boolean = false;
  faltanDatos: boolean = false;
  seleccionProducto: boolean = false;
  seleccionCantidad: boolean = false;
  seleccionRemito: boolean = false;
  productos: Producto[];
  selectedProducto: Producto;
  entrada: Entrada;

  constructor(private service: SanJoseService) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.service.getProductos().subscribe(response => {
      this.productos = response;
    });
  }

  getCodigoSelectedProducto(producto){
    this.selectedProducto = this.productos.find(p => p.nombre == producto);
    this.seleccionProducto = true;
    this.faltanDatos = false;
  }

  changeCantidad(){
    this.cantidad.value ? this.seleccionCantidad = true : this.seleccionCantidad = false;
    this.faltanDatos = false;
  }

  changeRemito(){
    this.remito.value ? this.seleccionRemito = true : this.seleccionRemito = false;
    this.faltanDatos = false;
  }

  postEntrada(){
    if(this.seleccionProducto && this.seleccionCantidad && this.seleccionRemito)
    {
      let ent:Entrada = {
        entradaId: 0,
        producto: this.selectedProducto,
        productoId: this.selectedProducto.productoId,
        cantidad: this.cantidad.value,
        remito: this.remito.value,
        fecha: new Date(),
      }
      this.service.addEntrada(ent).subscribe(response => {
        this.entradaCargada = true;
      });
    }
    else
    {
      this.faltanDatos = true;
    }
  }


  limpiar(){
    this.seleccionProducto = false;
    this.seleccionCantidad = false;
    this.seleccionRemito = false;
    this.faltanDatos = false;
    this.entradaCargada = false;
    this.productoId.setValue('');
    this.cantidad.setValue('');
    this.remito.setValue('');
    this.ngOnInit();
  }

  private initFormGroup(): void {
    this.form = new FormGroup({
      'productoId': this.productoId,
      'cantidad': this.cantidad,
      'remito': this.remito,
    });
  }
}
