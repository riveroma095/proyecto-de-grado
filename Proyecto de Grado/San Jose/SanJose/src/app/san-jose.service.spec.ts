import { TestBed } from '@angular/core/testing';

import { SanJoseService } from './san-jose.service';

describe('SanJoseService', () => {
  let service: SanJoseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SanJoseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
